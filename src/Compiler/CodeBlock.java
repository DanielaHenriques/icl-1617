package Compiler;

import java.util.ArrayList;

public class CodeBlock {

	ArrayList<String> code;

	public CodeBlock() {
		code = new ArrayList<String>(100);
	}

	public void emit_push(int n) {
		code.add("sipush "+n);
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}

	void dumpHeader() {
		System.out.println(".class public Demo");
		System.out.println(".super java/lang/Object");
		System.out.println("");
		System.out.println(";");
		System.out.println("; standard initializer");
		System.out.println(".method public <init>()V");
		System.out.println("   aload_0");
		System.out.println("   invokenonvirtual java/lang/Object/<init>()V");
		System.out.println("   return");
		System.out.println(".end method");
		System.out.println("");
		System.out.println(".method public static main([Ljava/lang/String;)V");
		System.out.println("       ; set limits used by this method");
		System.out.println("       .limit locals 10");
		System.out.println("       .limit stack 256");
		System.out.println("");
		System.out.println("       ; setup local variables:");
		System.out.println("");
		System.out.println("       ;    1 - the PrintStream object held in java.lang.System.out");
		System.out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		System.out.println("");
		System.out.println("       ; place your bytecodes here");
		System.out.println("       ; START");
		System.out.println("");
	}

	void dumpFooter() {
		System.out.println("       ; END");
		System.out.println("");
		System.out.println("");		
		System.out.println("       ; convert to String;");
		System.out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		System.out.println("       ; call println ");
		System.out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		System.out.println("");		
		System.out.println("       return");
		System.out.println("");		
		System.out.println(".end method");
	}

	void dumpCode() {
		for( String s : code )
			System.out.println("       "+s);
	}

	public void dump() {
		dumpHeader();
		dumpCode();
		dumpFooter();
	}
}
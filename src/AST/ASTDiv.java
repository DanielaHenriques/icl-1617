package AST;

import Compiler.CodeBlock;

public class ASTDiv implements ASTNode {

	ASTNode left, right;

	public ASTDiv(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval() {
		return left.eval() / right.eval();
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_div();
	}

	@Override
	public String toString() {
		return left.toString() + " / " + right.toString();
	}
}

package AST;

import Compiler.CodeBlock;

public interface ASTNode {

	int eval(); 
	
	void compile(CodeBlock code);

}


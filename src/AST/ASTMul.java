package AST;

import Compiler.CodeBlock;

public class ASTMul implements ASTNode {

	ASTNode left, right;

	public ASTMul(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval() {
		return left.eval() * right.eval();
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_mul();		
	}

	@Override
	public String toString() {
		return left.toString() + " * " + right.toString();
	}
}

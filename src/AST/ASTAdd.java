package AST;

import Compiler.CodeBlock;

public class ASTAdd implements ASTNode {

	ASTNode left, right;

	public ASTAdd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval() {
		return left.eval() + right.eval();
	}
	
	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		code.emit_add();
	}

	@Override
	public String toString() {
		return left.toString() + " + " + right.toString();
	}
}

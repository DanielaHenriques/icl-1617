package Tests;

import Main.Console;
import Parser.ParseException;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

public class ParserTester {

	private void testCase(String expression) throws ParseException {
		assertTrue(Console.accept(expression));		
	}
	
	private void testNegativeCase(String expression) throws ParseException {
		assertFalse(Console.accept(expression));
	}
	
	private void testCaseValue(String expression, int value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	private void testNegativeCaseValue(String expression, int value) throws ParseException {
		assertFalse(Console.acceptCompare(expression,value));
	}

	@Test
	public void test01() throws Exception {
		testCase("1\n");
	}

	@Test
	public void test02ArithmeticOps() throws Exception {
		testCase("1+2\n");
		testCase("1-2-3\n");
		testCase("4*2\n");
		testCase("4/2/2\n");
	}
	
	@Test
	public void test03Error01() throws Exception {
		testNegativeCase("1++1\n");
		testNegativeCase("-1+2\n");
		testNegativeCase("*2\n");
		testNegativeCase("4/+2/2\n");
	}
	
	@Test
	public void testsLabClass01() throws Exception {
		testCase("true\n");
		testCase("false\n");
		testCase("11 < 22\n");
		testCase("11 > 22\n");
		testCase("11 == 22\n");
		testCase("3*5 != 1+2 == true\n");
		testCase("1 == 2 && 3 == 4\n");
		testCase("1 == 2 || 3 == 4 && xpto \n");
		testCase("!(1 == 2) && xpto \n");
		testNegativeCase("< 11\n");
		testNegativeCase("11 >\n");
		testNegativeCase("<= 11\n");
		testNegativeCase("&& A\n");
	}

	@Test
	public void testsLabClass02() throws Exception {
		testCaseValue("1+2\n",3);
		testCaseValue("1-2-3\n",-4);
		testCaseValue("4*2\n",8);
		testCaseValue("4/2/2\n",1);
	}
}










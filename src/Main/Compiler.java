package Main;

import Compiler.*;
import Parser.Parser;
import AST.ASTNode;

class Compiler {

  public static void main(String args[]) {
    Parser parser = new Parser(System.in);
    ASTNode exp;

    try {
      exp = parser.Start();

      CodeBlock code = new CodeBlock();
      exp.compile(code);

      code.dump();

    } catch (Exception e) {
      System.out.println ("Syntax Error!");
    }
  }
}
